package mituapps.com.pensionescolombia.presentation.utils.navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.Person;
import mituapps.com.pensionescolombia.presentation.ui.activities.BulletinActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.Contact.ListWithImageActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.Contact.SocialNetworksActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.LinkInterestActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.InformationListActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.services.ServiceActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.team.TeamDetailActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre.PensionesColombiaActivity;

/**
 * Created by Andres Rubiano Del Chiaro on 18/10/2016.
 */

public class Navigator {

    public Navigator() {
    }

    public void toMenuActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            Intent intent = getIntent(context, MenuActivity.class);
            startNewActivity(context, intent);
        }
    }

    public void toBulletinActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, BulletinActivity.class));
        }
    }

    public void toLinkInterestActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, LinkInterestActivity.class));
        }
    }

    public void toPhonesActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, ListWithImageActivity.class));
        }
    }

    public void toSocialNetworkActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, SocialNetworksActivity.class));
        }
    }

    public void toPensionesColombiaInfoActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, PensionesColombiaActivity.class));
        }
    }

    public void toServiceActivity(@NonNull Context context){
        if (isContextNotNull(context)){
            startNewActivity(context, getIntent(context, ServiceActivity.class));
        }
    }

    private Intent getIntent(Context context, @NonNull Class goClass){
        return getIntent(context, goClass, null);
    }

    private Intent getIntent(Context context, @NonNull Class goClass, Bundle bundle){
        Intent intent = new Intent(context, goClass);
        if (null != bundle){
            intent.putExtras(bundle);
        }
        return intent;
    }

    private void startNewActivity(@NonNull Context context, @NonNull Intent intent){
        context.startActivity(intent);
    }

    //Validate if context is not null
    private boolean isContextNotNull(Context context) {
        if (null != context){
            return true;
        }
        throw new NullPointerException();
    }

    public void toReclamacionAportes(@NonNull Context context) {
        ArrayList<String> contain = new ArrayList<>();
        contain.add(context.getString(R.string.info_servicios_reclamacion_indemnizacion));
        contain.add(context.getString(R.string.info_servicios_reclamacion_devolucion));

        Intent intent = new Intent(context, InformationListActivity.class);
        intent.putExtra(InformationListActivity.TITLE_PAGE, context.getString(R.string.menu_servicios_reclamacion_aporte));
        intent.putExtra(InformationListActivity.ARRAY_LIST, R.array.reclamacion_aportes);
        intent.putStringArrayListExtra(InformationListActivity.ARRAY_CONTENT, contain);

        startNewActivity(context, intent);
    }

    public void toBusquedaPension(Context context) {
        ArrayList<String> contain2 = new ArrayList<>();
        Intent intent2 = new Intent(context, InformationListActivity.class);
        intent2.putExtra(InformationListActivity.TITLE_PAGE, context.getString(R.string.menu_servicios_busqueda_pension));
        intent2.putExtra(InformationListActivity.ARRAY_LIST, R.array.busqueda_tiempo);
        contain2.add(context.getString(R.string.info_servicios_busqueda_tiempo_reconstruccion));
        contain2.add(context.getString(R.string.info_servicios_busqueda_tiempo_certificado));
        contain2.add(context.getString(R.string.info_servicios_busqueda_tiempo_estudio));
        intent2.putStringArrayListExtra(InformationListActivity.ARRAY_CONTENT, contain2);
        context.startActivity(intent2);
    }

    public void toAfiliacionRegimen(Context context) {
        ArrayList<String> contain3 = new ArrayList<>();
        Intent intent3 = new Intent(context, InformationListActivity.class);
        intent3.putExtra(InformationListActivity.TITLE_PAGE, context.getString(R.string.menu_servicios_afiliacion_regimen));
        intent3.putExtra(InformationListActivity.ARRAY_LIST, R.array.afiliacion_regimen);
        contain3.add(context.getString(R.string.info_servicios_afiliacio_regimen_afiliacion));
        contain3.add(context.getString(R.string.info_servicios_afiliacio_regimen_pago_aportes));
        contain3.add(context.getString(R.string.info_servicios_afiliacio_regimen_calculo));
        intent3.putStringArrayListExtra(InformationListActivity.ARRAY_CONTENT, contain3);
        context.startActivity(intent3);
    }

    public void toDemanda(MenuActivity context) {
        TeamDetailActivity.createInstance(context, new Person(context.getString(R.string.menu_servicios_demanda_laboral),
                context.getString(R.string.info_servicios_demanda_laboral), R.drawable.demanda_laboral));
    }
}
