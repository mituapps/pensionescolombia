package mituapps.com.pensionescolombia.presentation.exceptions;

import android.util.Log;

/**
 * Created by Andres Rubiano on 09/11/2016.
 */

public class NullPenColException extends NullPointerException {

    private static final String ERROR_ID = "error";

    public NullPenColException(String s) {
        super(s);
        Log.e(ERROR_ID, s);
    }
}
