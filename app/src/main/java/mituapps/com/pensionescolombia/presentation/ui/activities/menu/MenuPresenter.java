package mituapps.com.pensionescolombia.presentation.ui.activities.menu;

/**
 * Created by andres.rubiano on 22/09/2017.
 */

public interface MenuPresenter {

    void onResume();

    void onDestroy();
}
