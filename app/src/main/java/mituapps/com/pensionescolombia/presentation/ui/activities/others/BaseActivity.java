package mituapps.com.pensionescolombia.presentation.ui.activities.others;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.presentation.exceptions.NullPenColException;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre.WeAreActivity;
import mituapps.com.pensionescolombia.presentation.ui.recycler.MarginDecoration;
import mituapps.com.pensionescolombia.presentation.ui.recycler.RecyclerClickListener;
import mituapps.com.pensionescolombia.presentation.ui.recycler.RecyclerTouchListener;
import mituapps.com.pensionescolombia.presentation.utils.animation.ItemAnimatorFactory;
import mituapps.com.pensionescolombia.presentation.utils.navigation.Navigator;

public class BaseActivity extends AppCompatActivity {

    protected Navigator mNavigator;

    public boolean bIsLargeScreen = false;
    private final static boolean IS_LARGE_SCREEN = true;
    private final static boolean IS_NOT_LARGE_SCREEN = false;

    private final static String URI_TEL = "tel";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNavigator();
    }

    protected void initNavigator() {
        if (mNavigator == null)
            mNavigator = new Navigator();
    }

    private void changeOrientation(@NonNull int type){
        setRequestedOrientation(type);
    }

    protected void setLargeScreen(@NonNull boolean flag){
        bIsLargeScreen = flag;
    }

    protected void hideView(@NonNull View v){v.setVisibility(View.GONE);}

    protected void showView(@NonNull View v){v.setVisibility(View.VISIBLE);}

    protected void addFragment(@NonNull int layout, @NonNull Fragment fragment){
        addFragment(layout, fragment, null);
    }

    protected void addFragment(@NonNull int layout,
                               @NonNull Fragment fragment,
                               @Nullable Bundle bundle){
        if (null != bundle){
            fragment.setArguments(bundle);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(layout, fragment);
        transaction.commit();
    }

    protected void showSnackbarMessage(@NonNull ViewGroup activity, @NonNull String message){
        Snackbar
                .make(activity, message, Snackbar.LENGTH_LONG)
                .show();
    }

    protected void showSnackbarMessage(@NonNull ViewGroup activity,
                                       @NonNull String message,
                                       @NonNull String infoClick,
                                       @NonNull View.OnClickListener onClickListener){
        Snackbar
                .make(activity, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(infoClick, onClickListener)
                .show();
    }

    protected boolean getStatusConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    protected void injectButterKnifeView(){
        ButterKnife.bind(this);
    }

    protected boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    protected void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    protected void initActionBar(@NonNull Toolbar toolbar) {
        if (toolbar == null) return;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Title
        //getSupportActionBar().setTitle("title");
    }

    protected void setUpRecycler(@NonNull RecyclerView recyclerView,
                                 RecyclerClickListener recyclerClickListener) {
        setUpRecycler(null, recyclerView, recyclerClickListener);
    }

    protected void setUpRecycler(@NonNull TextView view, @NonNull RecyclerView recyclerView) {
        setUpRecycler(view, recyclerView, null);
    }

    protected void setUpRecycler(@NonNull RecyclerView recyclerView) {
        setUpRecycler(null, recyclerView, null);
    }

    protected void setUpRecycler(@NonNull TextView view, @NonNull RecyclerView recyclerView,
                                 RecyclerClickListener recyclerClickListener) {
        if (null != view){
            ViewCompat.animate(view).alpha(1).start();
        }

        if (bIsLargeScreen) {
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        recyclerView.addItemDecoration(new MarginDecoration(this));
        recyclerView.setItemAnimator(ItemAnimatorFactory.slidein());
        recyclerView.setHasFixedSize(false);


        if (null != recyclerClickListener){
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, recyclerClickListener));
        }

    }

    protected Intent getIntentWithInfo(String title, String info){
        Intent intent = new Intent(this, WeAreActivity.class);
        intent.putExtra(MenuActivity.TITLE, title);
        intent.putExtra(MenuActivity.INFO, info);
        return intent;
    }

    protected void startNewActivity(Intent intent){
        startActivity(intent);
    }

    protected void makeCall(String phone){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(URI_TEL, phone, null));
        startActivity(intent);
    }

    protected void openUrlWithIntent(@NonNull String url){
        if (isValidString(url)){
            Intent intent= new Intent(Intent.ACTION_VIEW,Uri.parse(url));
            startActivity(intent);
        }
    }

    protected void initToolbar(Toolbar mToolbar) {
        if (mToolbar != null){
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            throw new NullPenColException("Toolbar is null");
        }
    }

    protected void sendEmail(@NonNull String emailTo, @NonNull String subject, @NonNull String body){
        try{
            if (isValidString(emailTo) && isValidString(subject) && isValidString(body)){
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","abc@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                //emailIntent.putExtra(Intent.EXTRA_EMAIL, addresses);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean isValidString(String text){
        return ((null != text) && (text.length() > 0)) ? true : false;
    }

    @Override
    protected void onDestroy() {
        try{
            ButterKnife.unbind(this);
        } catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
