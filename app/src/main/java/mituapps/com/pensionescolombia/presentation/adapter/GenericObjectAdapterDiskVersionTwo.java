package mituapps.com.pensionescolombia.presentation.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.GenericFragmentListener;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfo;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfoWithString;
import mituapps.com.pensionescolombia.presentation.ui.activities.services.ServiceListFragmentListener;
import mituapps.com.pensionescolombia.presentation.utils.media.ImageLoaderHelper;

/**
 * Created by Marcela on 1/09/2016.
 */
public class GenericObjectAdapterDiskVersionTwo
        extends RecyclerView.Adapter<GenericObjectAdapterDiskVersionTwo.ViewHolder> {

    private Context mContext;
    private List<GenericObjectInfoWithString> dataset;
    private GenericFragmentListener fragmentListener;
    private ImageLoaderHelper imageLoaderHelper;

    public GenericObjectAdapterDiskVersionTwo(Context mContext,
                                              GenericFragmentListener fragmentListener) {
        this.mContext = mContext;
        this.dataset = new ArrayList<>();
        this.fragmentListener = fragmentListener;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    public GenericObjectAdapterDiskVersionTwo(Context mContext,
                                              List<GenericObjectInfoWithString> dataset,
                                              GenericFragmentListener fragmentListener) {
        this.mContext = mContext;
        this.dataset = dataset;
        this.fragmentListener = fragmentListener;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_generic_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int pos){
        GenericObjectInfoWithString element = dataset.get(pos);
        imageLoaderHelper.getLoader().load(
                                        element.getPhoto(),
                                        holder.mImage);
        holder.mTxtTitle.setText(element.getTitle());
        holder.mTxtInfo.setText(element.getPreInfo());
        holder.setOnItemClick(element, fragmentListener);
    }

    public void addAll(List<GenericObjectInfoWithString> items) {
        int pos = getItemCount();
        dataset.addAll(items);
        notifyItemRangeInserted(pos, dataset.size());
    }

    @Override
    public int getItemCount(){
        if (null != dataset){
            return dataset.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_generic_list_image)
        ImageView mImage;

        @Bind(R.id.item_generic_list_title)
        TextView mTxtTitle;

        @Bind(R.id.item_generic_list_info)
        TextView mTxtInfo;

        public ViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setOnItemClick(final GenericObjectInfoWithString element,
                                   final GenericFragmentListener fragmentListener) {
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    fragmentListener.setMoreInfo(element);
                }
            });
        }
    }
}
