package mituapps.com.pensionescolombia.presentation.ui.activities.others;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.InformationObject;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;

/**
 * An activity representing a list of Informations. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link InformationDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class InformationListActivity extends BaseActivity {

    public static final String TITLE_PAGE = "title_page";
    public static final String ARRAY_LIST = "array_list";
    public static final String ARRAY_CONTENT = "array_content";
    private boolean mTwoPane;
    private InformationObject info;
    private List<String> data;

    @Bind(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_list);
        injectButterKnifeView();
        initActionBar(mToolbar);

        Intent intent = getIntent();
        info = new InformationObject(intent.getStringExtra(TITLE_PAGE),
                intent.getIntExtra(ARRAY_LIST, 0),
                intent.getStringArrayListExtra(ARRAY_CONTENT));

        mToolbar.setTitle(info.getTitle());

        View recyclerView = findViewById(R.id.information_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.information_detail_container) == null) return;
        mTwoPane = true;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        Resources res = getResources();
        String[] name = res.getStringArray(info.getListName());
        data = new ArrayList<>();
        for (int i = 0; i < name.length; i++) {
            data.add(name[i]);
        }
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(data));
    }

    private class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<String> mValues;

        public SimpleItemRecyclerViewAdapter(List<String> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.information_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
//            holder.mItem = mValues.get(position);
//            holder.mIdView.setText(mValues.get(position));
            holder.mContentView.setText(mValues.get(position));

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNewActivity(getIntentWithInfo(
                            getNextPageTitle(position),
                            info.getListContain().get(position)));
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }

    private String getNextPageTitle(int pos){
        return getResources().getStringArray(info.getListName())[pos];
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
