package mituapps.com.pensionescolombia.presentation.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.adapter.GenericObjectAdapterDiskVersionTwo;
import mituapps.com.pensionescolombia.presentation.model.GenericFragmentListener;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfoWithString;
import mituapps.com.pensionescolombia.presentation.model.Link;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre.WeAreActivity;

/**
 * Created by Andres Rubiano on 25/10/2016.
 */

public class LinkInterestFragment extends BaseFragment implements GenericFragmentListener {

    @Bind(R.id.recycler_fragment_link_interest) RecyclerView recyclerFragmentServiceList;
    @Bind(R.id.toolbar_we_are) Toolbar toolbar;

    private GenericObjectAdapterDiskVersionTwo adapter;
    private List<GenericObjectInfoWithString> dataInfo;

    public LinkInterestFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_link_interest, container, false);
        ButterKnife.bind(this, view);
        initActionBar(toolbar);
        initData();
        initRecyclerView();
        initAdapter();
        return view;
    }

    private void initData() {
        dataInfo = new ArrayList<>();

        ArrayList<Link> dataServer = Link.getLinks();
        for (Link link : dataServer) {
            dataInfo.add(getNewObjectInfo(
                    link.getName(),
                    link.getUrlImage(),
                    link.getUrlPage(),
                    link.getUrlPage()));
        }
    }

    private GenericObjectInfoWithString getNewObjectInfo(String title, String image, String preInfo, String info) {
        GenericObjectInfoWithString object = new GenericObjectInfoWithString();
        object.setTitle(title);
        object.setPhoto(image);
        object.setInfo(info);
        object.setPreInfo(preInfo);
        return object;
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = new GenericObjectAdapterDiskVersionTwo(getActivity().getApplicationContext(), this);
            recyclerFragmentServiceList.setAdapter(adapter);
            setUpToolbarAnimation(toolbar, new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // Fire item animator
                    adapter.addAll(dataInfo);

                    // Animate fab
//                ViewCompat.animate(mFab).setStartDelay(600)
//                        .setDuration(400).scaleY(1).scaleX(1).start();

                }
            });
        }
    }

    private void initRecyclerView() {
        setUpRecycler(recyclerFragmentServiceList);
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
//        recyclerFragmentServiceList.setLayoutManager(mLayoutManager);
     /*   recyclerFragmentServiceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerFragmentServiceList.addItemDecoration(new LinkInterestFragment.GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerFragmentServiceList.setItemAnimator(new DefaultItemAnimator());
        recyclerFragmentServiceList.setAdapter(adapter);*/

//        recyclerFragmentServiceList.setAdapter(adapter);
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private Intent getIntentWithInfo(String title, String info) {
        Intent intent = new Intent(getActivity(), WeAreActivity.class);
        intent.putExtra(MenuActivity.TITLE, title);
        intent.putExtra(MenuActivity.INFO, info);
        return intent;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void setMoreInfo(GenericObjectInfoWithString genericObjectInfo) {
        if (null != genericObjectInfo) {
            String url = genericObjectInfo.getInfo();
            if (null != url) {
                try {
                    intentToUrl(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
