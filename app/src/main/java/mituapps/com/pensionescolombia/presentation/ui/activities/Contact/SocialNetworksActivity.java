package mituapps.com.pensionescolombia.presentation.ui.activities.Contact;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;

public class SocialNetworksActivity extends BaseActivity {

    private static final String URL_FACEBOOK = "https://www.facebook.com/pcolombia/";
    private static final String URL_TWITTER = "https://twitter.com/pcolombia";
    private static final String URL_LINKEDIN = "https://co.linkedin.com/in/pensiones-colombia-26b94229/es";
    private static final String URL_YOUTUBE = "https://www.youtube.com/channel/UCQUXuliWi_wUwMBD3HCTWkA/videos";

    @Bind(R.id.toolbarSocialNetwork) Toolbar toolbarSocialNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_networks);
        injectButterKnifeView();
        initActionBar(toolbarSocialNetwork);
    }

    @OnClick({R.id.ivFacebook, R.id.ivLinkedIn, R.id.ivYoutube, R.id.ivTwitter})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivFacebook:
                onFacebookClick();
                break;
            case R.id.ivLinkedIn:
                onLinkedInClick();
                break;
            case R.id.ivYoutube:
                onYoutubeClick();
                break;
            case R.id.ivTwitter:
                onTwitterClick();
                break;
        }
    }

    private void onTwitterClick() {
        openUrlWithIntent(URL_TWITTER);
    }

    private void onYoutubeClick() {
        openUrlWithIntent(URL_YOUTUBE);
    }

    private void onLinkedInClick() {
        openUrlWithIntent(URL_LINKEDIN);
    }

    private void onFacebookClick() {
        openUrlWithIntent(URL_FACEBOOK);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
