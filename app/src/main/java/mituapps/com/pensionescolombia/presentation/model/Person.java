package mituapps.com.pensionescolombia.presentation.model;

/**
 * Created by Marcela on 5/09/2016.
 */
public class Person {

    private String name;
    private String rol;
    private int idDrawable;

    public Person(String name, String rol, int idDrawable) {
        this.name = name;
        this.rol = rol;
        this.idDrawable = idDrawable;
    }

    public String getName() {
        return name;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public String getRol() {
        return rol;
    }
}
