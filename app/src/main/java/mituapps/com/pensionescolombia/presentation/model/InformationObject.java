package mituapps.com.pensionescolombia.presentation.model;

import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 2/09/2016.
 */
public class InformationObject {

    private String title;
    private int listName;
    private List<String> listContain;

    public InformationObject(String title, int listName, List<String> listContain) {
        this.title = title;
        this.listName = listName;
        this.listContain = listContain;
    }

    public String getTitle() {
        return title;
    }

    public int getListName() {
        return listName;
    }

    public List<String> getListContain() {
        return listContain;
    }
}
