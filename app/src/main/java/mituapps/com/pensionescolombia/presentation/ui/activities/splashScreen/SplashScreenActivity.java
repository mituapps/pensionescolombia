package mituapps.com.pensionescolombia.presentation.ui.activities.splashScreen;

import android.os.Bundle;

import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;

public class SplashScreenActivity extends BaseActivity implements SplashScreenView {

    private final long SPLASH_SCREEN_DELAY = 2000;
    private SplashScreenPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        presenter = new SplashScreenPresenterImpl(this);
    }

    @Override
    public void goToLoginView() {
        mNavigator.toMenuActivity(SplashScreenActivity.this);
    }

    @Override
    public void closeCurrentView() {
        SplashScreenActivity.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public long getTimeToWait() {
        return SPLASH_SCREEN_DELAY;
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
