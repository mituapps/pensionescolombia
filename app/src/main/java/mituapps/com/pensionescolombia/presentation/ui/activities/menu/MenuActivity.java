package mituapps.com.pensionescolombia.presentation.ui.activities.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.Person;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.InformationListActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.services.ServiceActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.team.TeamDetailActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.team.TeamListActivity;

public class MenuActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MenuView {

    public static final String TITLE = "title";
    public static final String INFO = "info";

    @BindString(R.string.menu_general_quienessomos) String mWeAre;
    @BindString(R.string.text_quienessomos) String mWhoWeAre;
    @Bind(R.id.drawer_layout) DrawerLayout mDrawer;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.nav_view) NavigationView navView;

    private MenuPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        injectButterKnifeView();

        presenter = new MenuPresenterImpl(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_pencol:
                    mNavigator.toPensionesColombiaInfoActivity(MenuActivity.this);
                    break;

            case R.id.nav_pension:
                    mNavigator.toServiceActivity(MenuActivity.this);
                    break;

            case R.id.nav_reclamacion_aportes:
                    mNavigator.toReclamacionAportes(MenuActivity.this);
                    break;

            case R.id.nav_busqueda_pension:
                    mNavigator.toBusquedaPension(MenuActivity.this);
                    break;

            case R.id.nav_afiliacion_regimen:
                    mNavigator.toAfiliacionRegimen(MenuActivity.this);
                    break;

            case R.id.nav_equipo_de_trabajo:
                    startNewActivity(new Intent(MenuActivity.this, TeamListActivity.class));
                    break;

            case R.id.nav_demanda:
                    mNavigator.toDemanda(MenuActivity.this);
                    break;

            case R.id.nav_registro_boletin:
                    mNavigator.toBulletinActivity(MenuActivity.this);
                    break;

            case R.id.nav_enlaces_interes:
                    mNavigator.toLinkInterestActivity(MenuActivity.this);
                    break;

            case R.id.nav_telefonos:
                    mNavigator.toPhonesActivity(MenuActivity.this);
                    break;

            case R.id.nav_redes_sociales:
                    mNavigator.toSocialNetworkActivity(MenuActivity.this);
                    break;
        }

        closeNavDrawer();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    private void closeNavDrawer() {
        mDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void openNavDrawer() {
        mDrawer.openDrawer(GravityCompat.START, true);
    }

    @Override
    public void initActionNavigation() {
        navView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initActionDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void addOptionsMenu() {
        initActionDrawer();
    }

    @Override
    public void initActionBar() {
        initActionBar(toolbar);
    }
}
