package mituapps.com.pensionescolombia.presentation.ui.activities.others;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by Marcela on 31/08/2016.
 */
public class PensionesColombiApp extends Application {

    private String FIREBASE_CHILD = "";
    private String FIREBASE_URL = "";

    Firebase firebase;

    @Override
    public void onCreate() {
        super.onCreate();
//        Firebase.setAndroidContext(PensionesColombiApp.this);
//        Firebase.getDefaultConfig().setPersistenceEnabled(true);
//        firebase = new Firebase(FIREBASE_URL).child(FIREBASE_CHILD);
    }

    public Firebase getFirebase() {
        return firebase;
    }
}
