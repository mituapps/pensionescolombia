package mituapps.com.pensionescolombia.presentation.model;

import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import mituapps.com.pensionescolombia.R;

/**
 * Created by Andres Rubiano Del Chiaro on 5/09/2016.
 */
public class People {

    private static final int SIZE_PEOPLE = 4;
    private static final int SIZE_SITES  = 5;

    private static final String[] names = {
            "Hector Del Chiaro Martínez",
            "Rodrigo Orozco Del Gordo",
            "Rafael Brodmeier Valencia",
            "Iván Reyes Coronel"
    };

    private static final String[] rol = {
            "Gerente General",
            "Director Administrativo",
            "Director Comercial",
            "Director Operativo"
    };

    private static final int[] idImage = {
            R.drawable.hector_del_chiaro,
            R.drawable.rodrigo_orozco,
            R.drawable.rafael_brodmeier,
            R.drawable.ivan_reyes
    };

    private static final String[] countries = {
            "Colombia",
            "Colombia",
            "Inglaterra",
            "Estados Unidos",
            "Estados Unidos"
    };

    public static final String[] phones = {
            "+57 5 344 0731",
            "+57 5 349 2006",
            "+44 020 337 17740",
            "+1 954 6786942",
            "+1 954 3535707"
    };

    private static final int[] flags = {
            R.drawable.colombia_flag,
            R.drawable.colombia_flag,
            R.drawable.uk_flag,
            R.drawable.usa_flag,
            R.drawable.usa_flag
    };

    /**
     * Generate info about Pensiones Colombia's team
     * @return list with info
     */
    public static List<Person> getPeople(){
        List<Person> people = new ArrayList<>();
        for(int info = 0; info < SIZE_PEOPLE; info++ ){
            people.add(getPerson(names[info],
                                rol[info],
                                idImage[info]));
        }
        return people;
    }

    public static ArrayList<Person> getSites(){
        ArrayList<Person> people = new ArrayList<>();
        for(int info = 0; info < SIZE_SITES; info++ ){
            people.add(getPerson(countries[info],
                    phones[info],
                    flags[info]));
        }
        return people;
    }

    private static Person getPerson(String name, String rol, int idDrawable){
        return new Person(name, rol, idDrawable);
    }
}
