package mituapps.com.pensionescolombia.presentation.ui.activities.services;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;

public class ServiceActivity extends BaseActivity {

    @Bind(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        injectButterKnifeView();
        initActionBar(mToolbar);
        injectButterKnifeView();
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
