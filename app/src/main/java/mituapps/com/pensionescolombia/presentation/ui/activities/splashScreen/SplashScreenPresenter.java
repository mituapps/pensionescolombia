package mituapps.com.pensionescolombia.presentation.ui.activities.splashScreen;

/**
 * Created by andres.rubiano on 22/09/2017.
 */

interface SplashScreenPresenter {

    void initTimer();

    void onDestroy();

    void onResume();
}
