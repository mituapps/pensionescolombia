package mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.bluejamesbond.text.DocumentView;
import com.bluejamesbond.text.style.TextAlignment;

import butterknife.Bind;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;

public class WeAreActivity extends BaseActivity {

    @Bind(R.id.txtInfo) DocumentView txtInfo;
    @Bind(R.id.toolbar_we_are) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_we_are);

        injectButterKnifeView();
        initActionBar(toolbar);

        Intent intent = getIntent();
        getSupportActionBar().setTitle(intent.getStringExtra(MenuActivity.TITLE));
        txtInfo.setText(intent.getStringExtra(MenuActivity.INFO));
        txtInfo.getDocumentLayoutParams().setTextAlignment(TextAlignment.JUSTIFIED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            WeAreActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
