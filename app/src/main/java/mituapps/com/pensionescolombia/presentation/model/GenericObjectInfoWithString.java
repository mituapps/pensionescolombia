package mituapps.com.pensionescolombia.presentation.model;

/**
 * Created by Andres Rubiano DEl Chiaro on 1/09/2016.
 */
public class GenericObjectInfoWithString {
    private String title;
    private String preInfo;
    private String photo;
    private String info;

    public GenericObjectInfoWithString() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreInfo() {
        return preInfo;
    }

    public void setPreInfo(String preInfo) {
        this.preInfo = preInfo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
