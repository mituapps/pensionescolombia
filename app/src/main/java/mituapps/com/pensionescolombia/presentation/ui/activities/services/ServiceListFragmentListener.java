package mituapps.com.pensionescolombia.presentation.ui.activities.services;

import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfo;

/**
 * Created by Marcela on 1/09/2016.
 */
public interface ServiceListFragmentListener {

    void setMoreInfo(GenericObjectInfo genericObjectInfo);

}
