package mituapps.com.pensionescolombia.presentation.model;

/**
 * Created by Marcela on 1/09/2016.
 */
public class GenericObjectInfo {
    private int title;
    private int preInfo;
    private int photo;
    private int info;

    public GenericObjectInfo() {
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getPreInfo() {
        return preInfo;
    }

    public void setPreInfo(int preInfo) {
        this.preInfo = preInfo;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }
}
