package mituapps.com.pensionescolombia.presentation.ui.activities.splashScreen;

import android.os.Handler;

/**
 * Created by andres.rubiano on 22/09/2017.
 */
public class SplashScreenPresenterImpl implements SplashScreenPresenter {

    private SplashScreenView splashScreenView;
    private Handler handler;
    private Runnable runnable;

    public SplashScreenPresenterImpl(SplashScreenView splashScreenView) {
        this.splashScreenView = splashScreenView;
    }

    @Override
    public void initTimer() {
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                goToLoginPage();
            }
        }, splashScreenView.getTimeToWait());
    }

    private void goToLoginPage() {
        if (splashScreenView == null) return;
        splashScreenView.goToLoginView();
        splashScreenView.closeCurrentView();
    }

    @Override
    public void onDestroy() {
        splashScreenView = null;
        handler.removeCallbacks(runnable);
        handler = null;
        runnable = null;
    }

    @Override
    public void onResume() {
        initTimer();
    }
}
