package mituapps.com.pensionescolombia.presentation.ui.activities.team;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluejamesbond.text.DocumentView;
import com.bluejamesbond.text.style.TextAlignment;
import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.Person;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;

public class TeamDetailActivity extends BaseActivity {


    private static final String EXTRA_NAME = "team_name";
    private static final String EXTRA_ROL = "team_rol";
    private static final String EXTRA_DRAWABLE = "team_image";

    @Bind(R.id.txt_info_detail_image) DocumentView txtInfoDetailImage;
    @Bind(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_detail);
        injectButterKnifeView();
        initActionBar(mToolbar);

        Intent i = getIntent();
        String name = i.getStringExtra(EXTRA_NAME);
        String rol = i.getStringExtra(EXTRA_ROL);
        int idDrawable = i.getIntExtra(EXTRA_DRAWABLE, -1);
        txtInfoDetailImage.setText(rol);
        txtInfoDetailImage.getDocumentLayoutParams().setTextAlignment(TextAlignment.JUSTIFIED);
        CollapsingToolbarLayout collapser = (CollapsingToolbarLayout) findViewById(R.id.collapser);
        collapser.setTitle(name); // Cambiar título

        loadImageParallax(idDrawable);// Cargar Imagen
    }

    /**
     * Se carga una imagen aleatoria para el detalle
     */
    private void loadImageParallax(int id) {
        ImageView image = (ImageView) findViewById(R.id.image_paralax);
        // Usando Glide para la carga asíncrona
        Glide.with(this)
                .load(id)
                .centerCrop()
                .into(image);
    }

    public static void createInstance(Activity activity, Person person) {
        Intent intent = getLaunchIntent(activity, person);
        activity.startActivity(intent);
    }

    public static void createInstance(Activity activity, Person person, ImageView image, String reference) {
        Intent intent = getLaunchIntent(activity, person);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, image, reference);
            activity.startActivity(intent, options.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    public static void createInstance(Activity activity, Person person, Pair<View, String> avatar, Pair<View, String> name, Pair<View, String> position) {
        Intent intent = getLaunchIntent(activity, person);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, avatar, name, position);
            activity.startActivity(intent, options.toBundle());
        } else {
            activity.startActivity(intent);
        }
    }

    /**
     * Construye un Intent a partir del contexto y la actividad
     * de detalle.
     *
     * @param context Contexto donde se inicia
     * @param person  Identificador de la chica
     * @return Intent listo para usar
     */
    public static Intent getLaunchIntent(Context context, Person person) {
        Intent intent = new Intent(context, TeamDetailActivity.class);
        intent.putExtra(EXTRA_NAME, person.getName());
        intent.putExtra(EXTRA_ROL, person.getRol());
        intent.putExtra(EXTRA_DRAWABLE, person.getIdDrawable());
        return intent;
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
