package mituapps.com.pensionescolombia.presentation.ui.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.adapter.GenericObjectAdapterDisk;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfo;
import mituapps.com.pensionescolombia.presentation.model.Person;
import mituapps.com.pensionescolombia.presentation.ui.activities.menu.MenuActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.services.ServiceListFragmentListener;
import mituapps.com.pensionescolombia.presentation.ui.activities.team.TeamDetailActivity;
import mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre.WeAreActivity;

public class ServiceListFragment extends BaseFragment implements ServiceListFragmentListener {

    @Bind(R.id.recycler_fragment_service_list) RecyclerView recyclerFragmentServiceList;

    private GenericObjectAdapterDisk adapter;
    private List<GenericObjectInfo> dataInfo;

    public ServiceListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);
        ButterKnife.bind(this, view);
        initData();
        initRecyclerView();
        initAdapter();
        return view;
    }

    private void initData() {
        dataInfo = new ArrayList<>();

        dataInfo.add(getNewObjectInfo(R.string.title_servicios_pension_retroactivo,
                R.drawable.pension_retroactivo,
                R.string.pre_info_servicios_pension_retroactivo,
                R.string.info_servicios_pension_retroactivo));

        dataInfo.add(getNewObjectInfo(R.string.title_servicios_pension_invalidez,
                R.drawable.pen_invalidez,
                R.string.pre_info_servicios_pension_invalidez,
                R.string.info_servicios_pension_invalidez));

        dataInfo.add(getNewObjectInfo(R.string.title_servicios_pension_sobrevivientes,
                R.drawable.pension_sobrevivientes,
                R.string.pre_info_servicios_pension_sobrevivientes,
                R.string.info_servicios_pension_sobrevivientes));

        dataInfo.add(getNewObjectInfo(R.string.title_servicios_pension_ajuste,
                R.drawable.pension_ajustes,
                R.string.pre_info_servicios_pension_ajuste,
                R.string.info_servicios_pension_ajuste));

        dataInfo.add(getNewObjectInfo(R.string.title_servicios_pension_vejez,
                R.drawable.pension_vejez,
                R.string.pre_info_servicios_pension_vejez,
                R.string.info_servicios_pension_vejez));
    }

    private GenericObjectInfo getNewObjectInfo(int title, int image, int preInfo, int info) {
        GenericObjectInfo object = new GenericObjectInfo();
        object.setTitle(title);
        object.setPhoto(image);
        object.setInfo(info);
        object.setPreInfo(preInfo);
        return object;
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = new GenericObjectAdapterDisk(getActivity().getApplicationContext(), this);
            recyclerFragmentServiceList.setAdapter(adapter);
            //setUpToolbarAnimation(toolbar, new AnimatorListenerAdapter() {
              //  @Override
                //public void onAnimationEnd(Animator animation) {
                  //  super.onAnimationEnd(animation);

                    // Fire item animator
                    adapter.addAll(dataInfo);

                    // Animate fab
//                ViewCompat.animate(mFab).setStartDelay(600)
//                        .setDuration(400).scaleY(1).scaleX(1).start();

                //}
            //});
        }
    }

    private void initRecyclerView() {
        setUpRecycler(recyclerFragmentServiceList);

//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
//        recyclerFragmentServiceList.setLayoutManager(mLayoutManager);
        /*recyclerFragmentServiceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerFragmentServiceList.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerFragmentServiceList.setItemAnimator(new DefaultItemAnimator());
        recyclerFragmentServiceList.setAdapter(adapter);*/

//        recyclerFragmentServiceList.setAdapter(adapter);
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void setMoreInfo(GenericObjectInfo genericObjectInfo) {
        if (null != genericObjectInfo) {
            Person person = new Person(getString(genericObjectInfo.getTitle()),
                    getString(genericObjectInfo.getInfo()),
                    genericObjectInfo.getPhoto());
            TeamDetailActivity.createInstance(getActivity(), person);
        }
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
