package mituapps.com.pensionescolombia.presentation.ui.activities.team;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.adapter.TeamAdapter;
import mituapps.com.pensionescolombia.presentation.model.People;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;

public class TeamListActivity extends BaseActivity {

    private RecyclerView recycler;
    private LinearLayoutManager lManager;
    private CollapsingToolbarLayout collapser;

    @Bind(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        injectButterKnifeView();
        initActionBar(mToolbar);

        TeamAdapter adaptador = new TeamAdapter(this, People.getPeople());

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        //recycler.setHasFixedSize(false);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        recycler.setAdapter(adaptador);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}