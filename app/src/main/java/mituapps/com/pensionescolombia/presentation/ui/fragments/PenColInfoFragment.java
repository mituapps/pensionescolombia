package mituapps.com.pensionescolombia.presentation.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenColInfoFragment extends BaseFragment {

    public static final String ID_INFO = "idInfo";
    public static final String ID_TITLE = "idTitle";

    @Bind(R.id.txtInfo)
    TextView mTxtInfo;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    public PenColInfoFragment() {
    }

    public static PenColInfoFragment newInstance(Bundle arguments) {
        PenColInfoFragment fragment = new PenColInfoFragment();
        if (null != arguments) {
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pen_col_info, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        initActionBar(toolbar);
        setInfo();
        return view;
    }

    private void setInfo() {
        Bundle bundle = getArguments();
        String texto = bundle.getString(ID_INFO, "Vacío...");
        String title = bundle.getString(ID_TITLE, "Vacío...");
        toolbar.setTitle(texto);
        mTxtInfo.setText(texto);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
