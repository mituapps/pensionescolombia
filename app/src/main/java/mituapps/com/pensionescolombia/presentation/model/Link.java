package mituapps.com.pensionescolombia.presentation.model;

import java.util.ArrayList;

/**
 * Created by Andres Rubiano on 25/10/2016.
 */

public class Link {

    private String name;
    private String urlImage;
    private String urlPage;

    public Link(String name, String urlPage, String urlImage) {
        this.name = name;
        this.urlImage = urlImage;
        this.urlPage = urlPage;
    }

    public String getName() {
        return name;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getUrlPage() {
        return urlPage;
    }

    public static ArrayList<Link> getLinks(){
        ArrayList<Link> list = new ArrayList<>();
        list.add(new Link("MinSalud",
                            "https://www.minsalud.gov.co/",
                            "http://compromisosocial.rcn.com.co/wp-content/uploads/logoMinSalud-1.png"));
        list.add(new Link("Old Mutual",
                            "https://www.oldmutual.com.co/Paginas/Default.aspx",
                            "https://pbs.twimg.com/profile_images/634502148106133504/qR2byihp.png"));
        list.add(new Link("MinHacienda",
                            "https://www.minhacienda.gov.co/",
                            "http://www.findeter.gov.co/info/findeter/media/pub302464.jpg"));
        list.add(new Link("MinTrabajo",
                            "http://www.mintrabajo.gov.co/",
                            "https://upload.wikimedia.org/wikipedia/commons/6/6a/MinTrabajo_(Colombia)_logo.png"));
        list.add(new Link("La Unidad",
                            "http://www.ugpp.gov.co/",
                            "http://static.wixstatic.com/media/702a2d_a894c5168bd64bd180805ba6003b3837.png"));
        list.add(new Link("Protección",
                            "https://www.proteccion.com/",
                            "http://www.paginasamarillasdecundinamarca.com/wp-content/uploads/2013/09/PROTECCION-PENSIONE-SY-CESANTIAS.jpg"));
        list.add(new Link("Porvenir",
                            "https://www.porvenir.com.co/",
                            "http://tramitescolombia.co/wp-content/uploads/2015/08/Porvenir.gif"));
        list.add(new Link("Colfondos",
                            "https://www.colfondos.com.co/",
                            "http://decrimsas.com/images/headers/colfondos.jpg"));
        list.add(new Link("Colpensiones",
                            "https://colpensiones.gov.co/",
                            "http://colpensiones2015.deportescompensar.com/uploads/abba3d5c58eecf3f64d381bec0664a882d725356.png"));
        return list;
    }
}
