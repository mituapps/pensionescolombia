package mituapps.com.pensionescolombia.presentation.utils.media;

import android.widget.ImageView;

public interface ImageLoader {

    void load(String url,ImageView imageView);
    void loadLocal(String path, ImageView imageView);
}
