package mituapps.com.pensionescolombia.presentation.ui.activities.others;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mituapps.com.pensionescolombia.R;

/**
 * A fragment representing a single Information detail screen.
 * This fragment is either contained in a {@link InformationListActivity}
 * in two-pane mode (on tablets) or a {@link InformationDetailActivity}
 * on handsets.
 */
public class InformationDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

//    private DummyContent.DummyItem mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public InformationDetailFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.information_detail, container, false);

        // Show the dummy content as text in a TextView.
//        if (mItem != null) {
//            ((TextView) rootView.findViewById(R.id.information_detail)).setText(mItem.details);
//        }

        return rootView;
    }
}
