package mituapps.com.pensionescolombia.presentation.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfo;
import mituapps.com.pensionescolombia.presentation.model.GenericObjectInfoWithString;
import mituapps.com.pensionescolombia.presentation.ui.activities.services.ServiceListFragmentListener;
import mituapps.com.pensionescolombia.presentation.utils.media.ImageLoaderHelper;

/**
 * Created by Marcela on 1/09/2016.
 */
public class GenericObjectAdapterDisk extends RecyclerView.Adapter<GenericObjectAdapterDisk.ViewHolder> {

    private Context mContext;
    private List<GenericObjectInfo> dataset;
    private ServiceListFragmentListener fragmentListener;
    private ImageLoaderHelper imageLoaderHelper;

    public GenericObjectAdapterDisk(Context mContext, List<GenericObjectInfo> dataset, ServiceListFragmentListener fragmentListener) {
        this.mContext = mContext;
        this.dataset = dataset;
        this.fragmentListener = fragmentListener;
    }

    public GenericObjectAdapterDisk(Context mContext, ServiceListFragmentListener fragmentListener) {
        this.mContext = mContext;
        this.dataset = new ArrayList<>();
        this.fragmentListener = fragmentListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_generic_list, parent, false);
        return new ViewHolder(view);
    }

    public void addAll(List<GenericObjectInfo> items) {
        int pos = getItemCount();
        dataset.addAll(items);
        notifyItemRangeInserted(pos, dataset.size());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int pos){
        GenericObjectInfo element = dataset.get(pos);
        holder.mImage.setImageDrawable(ContextCompat.getDrawable(mContext, element.getPhoto()));
        holder.mTxtTitle.setText(mContext.getString(element.getTitle()));
        holder.mTxtInfo.setText(mContext.getString(element.getPreInfo()));
        holder.setOnItemClick(element, fragmentListener);
    }

    @Override
    public int getItemCount(){
        return dataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_generic_list_image)
        ImageView mImage;

        @Bind(R.id.item_generic_list_title)
        TextView mTxtTitle;

        @Bind(R.id.item_generic_list_info)
        TextView mTxtInfo;

        public ViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setOnItemClick(final GenericObjectInfo element, final ServiceListFragmentListener fragmentListener) {
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    fragmentListener.setMoreInfo(element);
                }
            });
        }
    }
}
