package mituapps.com.pensionescolombia.presentation.ui.activities.Contact;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.adapter.TeamAdapter;
import mituapps.com.pensionescolombia.presentation.exceptions.NullPenColException;
import mituapps.com.pensionescolombia.presentation.model.People;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;
import mituapps.com.pensionescolombia.presentation.ui.recycler.RecyclerClickListener;
import mituapps.com.pensionescolombia.presentation.ui.recycler.RecyclerTouchListener;

public class ListWithImageActivity extends BaseActivity {

    @Bind(R.id.reciclador)  RecyclerView mRecycler;
    @Bind(R.id.toolbar_we_are)     Toolbar mToolbar;
    @Bind(R.id.coordinator) CoordinatorLayout mCoordinator;
    private TeamAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        ButterKnife.bind(this);

        initToolbar(mToolbar);
        initAdapter();
        initRecycler();
        initOnItemClickListener();
    }

    private void initAdapter() {
        mAdapter = new TeamAdapter(this, People.getSites(), false);
    }

    private void initOnItemClickListener() {
        if (null != mRecycler) {
            mRecycler.addOnItemTouchListener(new RecyclerTouchListener(this, mRecycler, new RecyclerClickListener() {
                @Override
                public void onClick(View view, int position) {
                    makeCall(People.phones[position]);
                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
    }

    private void initRecycler() {
        // Get the recycler
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));

        if (null != mAdapter) {
            mRecycler.setAdapter(mAdapter);
        } else {
            throw new NullPenColException("Adapter is null in " + this.getLocalClassName());
        }
    }


}