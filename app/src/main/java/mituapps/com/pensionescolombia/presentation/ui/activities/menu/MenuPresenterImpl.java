package mituapps.com.pensionescolombia.presentation.ui.activities.menu;

/**
 * Created by andres.rubiano on 22/09/2017.
 */

public class MenuPresenterImpl implements MenuPresenter {

    private MenuView menuView;

    public MenuPresenterImpl(MenuView menuView) {
        this.menuView = menuView;
    }

    @Override
    public void onResume() {
        initComponents();
    }

    @Override
    public void onDestroy() {
        menuView = null;
    }

    private void initComponents() {
        menuView.initActionBar();
        menuView.addOptionsMenu();
        menuView.initActionNavigation();
        menuView.openNavDrawer();
    }
}
