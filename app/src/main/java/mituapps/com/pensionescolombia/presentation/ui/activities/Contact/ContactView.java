package mituapps.com.pensionescolombia.presentation.ui.activities.Contact;

import mituapps.com.pensionescolombia.presentation.view.BaseView;

/**
 * Created by Andres Rubiano on 09/11/2016.
 */

public interface ContactView extends BaseView {

    void enableInputs();
    void disableInputs();

    void onItemSelected(int pos);
}
