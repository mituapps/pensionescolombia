package mituapps.com.pensionescolombia.presentation.view;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface BulletinView extends BaseUI{

    void onError();
    void onSuccess(String message);
}
