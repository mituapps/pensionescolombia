package mituapps.com.pensionescolombia.presentation.model;

/**
 * Created by Marcela on 1/09/2016.
 */
public interface GenericFragmentListener {

    void setMoreInfo(GenericObjectInfoWithString genericObjectInfo);

}
