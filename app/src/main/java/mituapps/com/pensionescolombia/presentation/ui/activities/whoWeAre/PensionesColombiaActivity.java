package mituapps.com.pensionescolombia.presentation.ui.activities.whoWeAre;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.FrameLayout;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;
import mituapps.com.pensionescolombia.presentation.ui.fragments.PenColInfoFragment;

public class PensionesColombiaActivity extends BaseActivity implements OnTabSelectListener {

    @Bind(R.id.bottombar) BottomBar mBottombar;
    @Bind(R.id.containerInfo) FrameLayout containerInfo;
    @Bind(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pensiones_colombia);
        injectButterKnifeView();
        initActionBarListener();
        initActionBar(mToolbar);
    }

    private void initActionBarListener() {
        mBottombar.setOnTabSelectListener(PensionesColombiaActivity.this);
    }

    @Override
    public void onTabSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.bottombar_weare:
                addFragment(R.id.containerInfo, PenColInfoFragment.newInstance(getBundle("We are...")));
                break;

            case R.id.bottombar_mision:
                addFragment(R.id.containerInfo, PenColInfoFragment.newInstance(getBundle("Mision...")));
                break;

            case R.id.bottombar_vision:
                addFragment(R.id.containerInfo, PenColInfoFragment.newInstance(getBundle("Vision...")));
                break;
        }
    }

    private Bundle getBundle(String s) {
        Bundle bundle = new Bundle();
        bundle.putString(PenColInfoFragment.ID_INFO, s);
        return bundle;
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
