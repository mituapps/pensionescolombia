package mituapps.com.pensionescolombia.presentation.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.ui.activities.others.BaseActivity;
import mituapps.com.pensionescolombia.presentation.view.BulletinView;

public class BulletinActivity extends BaseActivity implements BulletinView {

    @Bind(R.id.inputNameBulletin)
    EditText inputNameBulletin;
    @Bind(R.id.inputEmailBulletin)
    EditText inputEmailBulletin;
    @Bind(R.id.progressBulletin)
    ProgressBar progressBulletin;
    @Bind(R.id.container_bulletin)
    LinearLayout containerBulletin;
    @Bind(R.id.wrapperNameBulletin)
    TextInputLayout wrapperNameBulletin;
    @Bind(R.id.wrapperEmailBulletin)
    TextInputLayout wrapperEmailBulletin;
    @Bind(R.id.onSendBulletin)
    Button onSendBulletin;
    @Bind(R.id.toolbar_we_are)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin);
        injectButterKnifeView();
        initToolbar(toolbar);
    }

    @OnClick(R.id.onSendBulletin)
    public void onClick() {
        showProgress();
        disableInputs();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showSnackbarMessage(containerBulletin, "¡Registro al boletín agregado satifactoriamente!");
                closeActivity();
            }
        }, 2000);
    }

    private void closeActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                close();
            }
        }, 2000);
    }

    private void close() {
        BulletinActivity.this.finish();
    }

    @Override
    public void hideProgress() {
        progressBulletin.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressBulletin.setVisibility(View.VISIBLE);
    }

    @Override
    public void showContainer() {

    }

    @Override
    public void hideContainer() {

    }

    @Override
    public void enableInputs() {
        setInputsState(true);
    }

    @Override
    public void disableInputs() {
        setInputsState(false);
    }

    private void setInputsState(boolean state) {
        inputEmailBulletin.setEnabled(state);
        inputNameBulletin.setEnabled(state);
        wrapperNameBulletin.setEnabled(state);
        wrapperEmailBulletin.setEnabled(state);
        onSendBulletin.setEnabled(state);
    }

    @Override
    public void onErrorMessage(String message) {
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public Context getContext() {
        return BulletinActivity.this;
    }

    @Override
    public void onError() {

    }

    @Override
    public void onSuccess(String message) {

    }
}
