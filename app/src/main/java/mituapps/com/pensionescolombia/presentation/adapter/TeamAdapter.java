package mituapps.com.pensionescolombia.presentation.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import mituapps.com.pensionescolombia.R;
import mituapps.com.pensionescolombia.presentation.model.Person;
import mituapps.com.pensionescolombia.presentation.ui.activities.team.TeamDetailActivity;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.SimpleViewHolder>
        implements ItemClickListener {
    private final Context context;
    private List<Person> items;
    private boolean isListened = true;

    public static class SimpleViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView nombre;
        public TextView rol;
        public ImageView avatar;
        public ItemClickListener listener;

        public SimpleViewHolder(View v, ItemClickListener listener) {
            super(v);

            nombre = (TextView) v.findViewById(R.id.list_item_name);
            rol = (TextView) v.findViewById(R.id.list_item_rol);
            avatar = (ImageView) v.findViewById(R.id.avatar);
            this.listener = listener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(v, getAdapterPosition(), avatar, nombre, rol);
        }
    }

    public TeamAdapter(Context context, List<Person> items) {
        this.context = context;
        this.items = items;
    }

    public TeamAdapter(Context context, List<Person> items, boolean listener) {
        this.context = context;
        this.items = items;
        this.isListened = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_team, viewGroup, false);
        return new SimpleViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder viewHolder, int i) {
        Person currentItem = items.get(i);
        viewHolder.nombre.setText(currentItem.getName());
        viewHolder.rol.setText(currentItem.getRol());

        Glide.with(viewHolder.avatar.getContext())
                .load(currentItem.getIdDrawable())
                .centerCrop()
                .into(viewHolder.avatar);
    }

    @Override
    public void onItemClick(View view, int position, ImageView image, TextView nameTeam, TextView positionTeam) {
        if (isListened) {
            TeamDetailActivity.createInstance(
                    //(Activity) context, items.get(position), image, context.getString(R.string.transition_avatar));
                    (Activity) context, items.get(position), new Pair<View, String>(image, context.getString(R.string.transition_avatar)),
                    new Pair<View, String>(nameTeam, context.getString(R.string.transition_name)),
                    new Pair<View, String>(positionTeam, context.getString(R.string.transition_position)));
        }
    }

    private Pair<View, String> getViewToTransfer(ImageView image, TextView nameTeam, TextView positionTeam) {
        /*arrayInfo.create((View) positionTeam, context.getString(R.string.transition_position));
        arrayInfo.create((View) positionTeam, context.getString(R.string.transition_name));
        arrayInfo.create((View) image, context.getString(R.string.transition_avatar));*/
        return null;
    }
}

interface ItemClickListener {
    void onItemClick(View view, int position, ImageView image, TextView nameTeam, TextView positionTeam);
}
